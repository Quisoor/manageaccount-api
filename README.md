# manageaccount-api

[![Quality Gate](https://sonarcloud.io/api/badges/gate?key=Quisoor_manageaccount-api)](https://sonarcloud.io/dashboard/index/Quisoor_manageaccount-api)
[![Comments (%)](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=comment_lines_density)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=comment_lines_density)
[![Open issues](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=open_issues)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=open_issues)
[![Code smells](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=code_smells)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=code_smells)
[![Technical debt](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=sqale_index)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=sqale_index)
[![Bugs](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=bugs)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=bugs)
[![Reliability remediation effort](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=reliability_remediation_effort)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=reliability_remediation_effort)
[![Coverage](https://sonarcloud.io/api/badges/measure?key=Quisoor_manageaccount-api&metric=coverage)](https://sonarcloud.io/component_measures?id=Quisoor_manageaccount-api&metric=coverage)