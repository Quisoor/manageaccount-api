/**
 * Sails Seed Settings
 * (sails.config.seeds)
 *
 * Configuration for the data seeding in Sails.
 *
 * For more information on configuration, check out:
 * http://github.com/frostme/sails-seed
 */
module.exports.seeds = {
    user:[
        {
            id: 1,
            mail: 'admin@mail.com',
            username: 'admin',
            password: 'pwd',
            person: {
                id: 1
            }
        }
    ],
    person:[
        {
            id:1,
            firstname: 'admin',
            lastname: 'admin',
            user: {
                id: 1
            }
        }
    ]
}
